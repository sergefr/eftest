﻿using System.Data.Entity;

namespace EFBasics
{
	public class Context : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Department> Departments { get; set; }

        public Context()
            // To specify an explicit connection or DB name call the base class constructor
            : base("localsql")
        { }       
    }
}
