﻿using System.ComponentModel.DataAnnotations;

namespace EFBasics
{
	public class Employee
    {
        public int Id { get; set; }
        [MaxLength(30)]
        public string Name { get; set; }

        public virtual Department Department { get; set; }
    }
}
