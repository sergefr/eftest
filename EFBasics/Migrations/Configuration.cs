namespace EFBasics.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<EFBasics.Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(EFBasics.Context context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            var departmentSD = new Department { Name = "Software development" };
            var departmentHR = new Department { Name = "HR" };
            context.Departments.AddOrUpdate(d => d.Name, departmentSD, departmentHR);

            // !!!
            context.SaveChanges();

            context.Employees.AddOrUpdate(e => e.Name, 
                new Employee { Name = "Sergey Efremov",
                    Department = context.Departments.FirstOrDefault(d => d.Name == departmentSD.Name) },
                new Employee { Name = "Afanasiy Nikitin",
                    Department = context.Departments.FirstOrDefault(d => d.Name == departmentHR.Name)
                }
            );           

        }
    }
}
