﻿using System;
using System.Linq;

namespace EFBasics
{
	class Program
    {
        static void AddItems(Context context)
        {
            var departmentSD = new Department { Name = "Software development" };
            var departmentHR = new Department { Name = "HR" };
            context.Departments.Add(departmentSD);
            context.Departments.Add(departmentHR);

            context.Employees.Add(new Employee { Name = "Sergey Efremov", Department = departmentSD });
            context.Employees.Add(new Employee { Name = "Afanasiy Nikitin", Department = departmentHR });
            
            context.SaveChanges();
        }

        static void EditItem(Context context)
        {
            var employee = context.Employees.First(e => e.Name.StartsWith("S"));
            employee.Name = "Sergey";            
            context.SaveChanges();
        }

        static void RemoveItem(Context context)
        {
            var employee = context.Employees.FirstOrDefault(e => e.Name.StartsWith("A"));
            if (employee != null)
            {
                context.Employees.Remove(employee);
                context.SaveChanges();
            }
        }

        static void QueryItems(Context context)
        {
            // context.Departments.Load();
            var employees = context.Employees.ToList();
            Console.WriteLine("All employees");
            foreach(var e in employees)
                Console.WriteLine("{0} - {1}", e.Name, e.Department != null ? e.Department.Name : "None");
        }

        static void Main(string[] args)
        {   
            using (var context = new Context()) // Context class is disposable
            {                
                // Uncomment the following line to print requests sent to the database
                // context.Database.Log += Console.WriteLine;               

                // Uncomment one item at a time
                // AddItems(context);
                // EditItem(context);
                // RemoveItem(context);
                
                QueryItems(context);
            }
			Console.ReadKey();           
        }
    }
}
